import screen_brightness_control as sbc
import argparse
from config import *

current_brightness = 100

arg_parser = argparse.ArgumentParser(
    prog=PROJECT_NAME,
    description=PROJECT_DESC,
    epilog="Happy coding!"
)

arg_parser.add_argument('-m', '--monitor', type=int, default=0, help='Select a specific monitor to change it\'s brigthness. By default selects the primary monitor.')
arg_parser.add_argument('-b', '--brightness', type=int, default=100, help='Select a brightness level.')
arg_parser.add_argument('-s', '--start_time', help='Select a specific time of the day to change the brightness.')
arg_parser.add_argument('-e', '--end_time', help='Select a specific time of the day to return the brightness to normal.')
arg_parser.add_argument('-k', '--kill_blue_light', help='Stops the blue light from the screen.')
arg_parser.add_argument('-i', '--interactive', help='Starts un interactive TUI panel') # Interactive mode
arg_parser.add_argument('-l', '--list_monitors', action='store_true', help='Get the names of all monitors on your system')

args = arg_parser.parse_args()

# TODO: Move the arg parser to a separate file
# TODO: use notiblocks for the notification stuff
# TODO: Ask for a root permission

def list_all_monitors():
    i = 0
    for current in sbc.list_monitors():
        print(f" ({i}) {current} ->  ({sbc.get_brightness(display=current)[0]}%)" + " |PRIMARY|" if i == 0 else "")
        i = i+1

def set_monitor_brightness(brightness_level: int, monitor: int):
    if brightness_level < 10:
        print(f"[!] We do not encourage setting the brightness level to {brightness_level}. Try something above 10%!")
        brightness_level = 10

    current_brightness = brightness_level
    sbc.set_brightness(current_brightness, display=monitor)
    print(f"[+] Successfully changed the brightness level to {brightness_level} for monitor {monitor}")

def kill_blue_light():
    pass

def main():
    if args.list_monitors:
        list_all_monitors()

    monitor = 0

    # It detects this even if we have not changed it
    if args.monitor:
        monitor = int(args.monitor)

    if args.brightness != 100 or (args.brightness == 100 and current_brightness != 100):
        set_monitor_brightness(args.brightness, monitor)

if __name__ == "__main__":
    main()